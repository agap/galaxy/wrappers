#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Saliha ZB
import argparse
from Bio import SearchIO
from Bio import SeqIO

parser = argparse.ArgumentParser(
    description="", formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument("-i", "--input", help="hmmer file")
parser.add_argument("-f", "--fasta", help="fasta file")  # Ajout de l'argument pour le fichier FASTA
parser.add_argument("-o", "--output", help="output file")
parser.add_argument("-s", "--species", help="species name")  # Ajout de l'argument pour le nom de l'espèce

args = parser.parse_args()

list_id = []
results = SearchIO.parse(args.input, "hmmer3-text")
for protein in results:
    hits = protein.hits
    if len(hits) > 0:
        for i in range(0, len(hits)):
            hit_id = hits[i].id
            list_id.append(hit_id)

# retrieve sequences from fasta file
sequence_dict = {} 
with open(args.fasta, "r") as fasta_file:
    for record in SeqIO.parse(fasta_file, "fasta"):
        sequence_dict[record.id] = record

# write all informations in a file with species code
with open(args.output, "w") as fp:
    for hit_id in list_id:
        record = sequence_dict.get(hit_id, "Sequence not found")
        record.id = record.id + "_" + args.species
        record.description = ""

        SeqIO.write(record, fp, 'fasta')

    
